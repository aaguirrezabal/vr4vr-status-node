/* 
 * File:   BaxterStateUpdater.cpp
 * Author: Andoni Aguirrezabal
 */

#include "BaxterStateUpdater.h"

BaxterStateUpdater::BaxterStateUpdater() :
    leftJointAngles({0, 0, 0, 0, 0, 0, 0}),
    rightJointAngles({0, 0, 0, 0, 0, 0, 0})
{
    firstJointStateReceived = false;
}

BaxterStateUpdater::~BaxterStateUpdater() {
}

bool BaxterStateUpdater::init() {
    // Load Joint State Subscriber
    jointStateSubscriber = nh.subscribe<sensor_msgs::JointState>("/robot/joint_states", 1,
                           &BaxterStateUpdater::jointStateCallback, this);

    return true;
}

void BaxterStateUpdater::jointStateCallback(const sensor_msgs::JointStateConstPtr& msg) {
    if(!firstJointStateReceived) {
        firstJointStateReceived = true;
    }

    // Left Joint Angles
    leftJointAngles[0] = msg->position[2];
    leftJointAngles[1] = msg->position[3];
    leftJointAngles[2] = msg->position[4];
    leftJointAngles[3] = msg->position[5];
    leftJointAngles[4] = msg->position[6];
    leftJointAngles[5] = msg->position[7];
    leftJointAngles[6] = msg->position[8];

    // Right Joint Angles
    rightJointAngles[0] = msg->position[9];
    rightJointAngles[1] = msg->position[10];
    rightJointAngles[2] = msg->position[11];
    rightJointAngles[3] = msg->position[12];
    rightJointAngles[4] = msg->position[13];
    rightJointAngles[5] = msg->position[14];
    rightJointAngles[6] = msg->position[15];
}

