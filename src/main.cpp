/* 
 * File:   main.cpp
 * Author: Andoni Aguirrezabal
 */
#include "BaxterStateUpdater.h"

#include <cstdlib>
#include <ctime>

#include "lsl_cpp.h"

using namespace std;

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "vr4vr_status_node");
    ros::NodeHandle nh;

    lsl::stream_info leftArmStreamInfo("Left Arm Joint States",
            "Joint Positions", 7);
    lsl::stream_info rightArmStreamInfo("Right Arm Joint States",
            "Joint Positions", 7);

    lsl::stream_outlet leftArmStreamOutlet(leftArmStreamInfo);
    lsl::stream_outlet rightArmStreamOutlet(rightArmStreamInfo);

    BaxterStateUpdater jointUpdater;
    jointUpdater.init();

    while(nh.ok()) {
        ros::spinOnce();

        leftArmStreamOutlet.push_sample(jointUpdater.getLeftJointStates());
        rightArmStreamOutlet.push_sample(jointUpdater.getRightJointStates());
    }

    return 0;
}

