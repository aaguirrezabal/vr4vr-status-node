/* 
 * File:   BaxterStateUpdater.h
 * Author: Andoni Aguirrezabal
 */

#ifndef BAXTERSTATEUPDATER_H
#define	BAXTERSTATEUPDATER_H

// ROS Defines
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>

class BaxterStateUpdater {
public:
    BaxterStateUpdater();
    ~BaxterStateUpdater();

private:
    //*********************
    //*** CONFIGURATION ***
    //*********************
    // ROS Node Handle
    ros::NodeHandle nh;
    
public:
    //*****************************
    //*** CONFIGURATION METHODS ***
    //*****************************
    /*! \brief Initialize the Baxter Interface
     * 
     * @return a BOOL representing if the initialization
     * was successful
     */
    bool init(void);
    
private:
    //********************
    //*** ROS HANDLERS ***
    //********************
    // Subscribers
    ros::Subscriber jointStateSubscriber;

    // Subscriber Callbacks
    void jointStateCallback(const sensor_msgs::JointStateConstPtr& msg);
    
    // Joint Information Variables
    bool firstJointStateReceived;
    
private:
    //************
    //*** DATA ***
    //************
    float leftJointAngles[7];
    float rightJointAngles[7];

public:
    //***************************
    //*** DATA ACCESS METHODS ***
    //***************************
    const float* getLeftJointStates(void) {
        return &leftJointAngles[0];
    };

    const float* getRightJointStates(void) {
        return &rightJointAngles[0];
    };
};

#endif	/* BAXTERSTATEUPDATER_H */

